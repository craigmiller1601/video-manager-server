data "keycloak_openid_client" "video_manager_converter_dev" {
  realm_id  = data.keycloak_realm.apps_dev.id
  client_id = "video-manager-converter"
}

data "keycloak_openid_client" "video_manager_converter_prod" {
  realm_id  = data.keycloak_realm.apps_prod.id
  client_id = "video-manager-converter"
}

data "keycloak_role" "video_manager_converter_access_role_dev" {
  realm_id  = data.keycloak_realm.apps_dev.id
  client_id = data.keycloak_openid_client.video_manager_converter_dev.id
  name = local.roles_common.access_name
}

data "keycloak_role" "video_manager_converter_access_role_prod" {
  realm_id  = data.keycloak_realm.apps_prod.id
  client_id = data.keycloak_openid_client.video_manager_converter_prod.id
  name = local.roles_common.access_name
}

import {
  to = keycloak_openid_client_service_account_role.video_manager_server_converter_access_role_dev
  id = "apps-dev/3286e1b2-5bd9-44aa-a31d-72d1098ab217/ce88d0b8-07bc-4eed-9066-153247200fa9/2580f310-2993-46ce-a713-dc2fab7cd174"
}

resource "keycloak_openid_client_service_account_role" "video_manager_server_converter_access_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  service_account_user_id = keycloak_openid_client.video_manager_server_dev.service_account_user_id
  client_id = data.keycloak_openid_client.video_manager_converter_dev.id
  role = local.roles_common.access_name
}

import {
  to = keycloak_openid_client_service_account_role.video_manager_server_converter_access_role_prod
  id = "apps-prod/0ea34c2e-ee3d-4b7a-a610-ae67fcdf73d2/ce38689c-8034-48c5-bb89-8927b7a33aa0/d4abb156-7d66-45f5-8dca-0556adb5e7b7"
}

resource "keycloak_openid_client_service_account_role" "video_manager_server_converter_access_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  service_account_user_id = keycloak_openid_client.video_manager_server_prod.service_account_user_id
  client_id = data.keycloak_openid_client.video_manager_converter_prod.id
  role = local.roles_common.access_name
}