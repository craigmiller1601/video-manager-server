locals {
  roles_common = {
    access_name = "access"
    edit_name = "EDIT"
    scan_name = "SCAN"
    admin_name = "ADMIN"
  }
}

import {
  to = keycloak_role.video_manager_server_access_role_dev
  id = "apps-dev/143c2b37-20fb-45f0-9a2a-a8bae1cfca27"
}

resource "keycloak_role" "video_manager_server_access_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.video_manager_server_dev.id
  name = local.roles_common.access_name
}

import {
  to = keycloak_role.video_manager_server_edit_role_dev
  id = "apps-dev/3eb11d7b-2c5a-423a-ac36-722e1e002ca7"
}

resource "keycloak_role" "video_manager_server_edit_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.video_manager_server_dev.id
  name = local.roles_common.edit_name
}

import {
  to = keycloak_role.video_manager_server_scan_role_dev
  id = "apps-dev/b3a4d69f-1ddd-4660-9816-45afec7c0a24"
}

resource "keycloak_role" "video_manager_server_scan_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.video_manager_server_dev.id
  name = local.roles_common.scan_name
}

import {
  to = keycloak_role.video_manager_server_admin_role_dev
  id = "apps-dev/6717bcc2-f790-4d79-b138-6597e9813242"
}

resource "keycloak_role" "video_manager_server_admin_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.video_manager_server_dev.id
  name = local.roles_common.admin_name
}

import {
  to = keycloak_role.video_manager_server_access_role_prod
  id = "apps-prod/6306eed0-6d0c-4961-a710-fc3a668a0fc5"
}

resource "keycloak_role" "video_manager_server_access_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.video_manager_server_prod.id
  name = local.roles_common.access_name
}

import {
  to = keycloak_role.video_manager_server_edit_role_prod
  id = "apps-prod/37516b53-e30f-44f2-b5b3-53b03a5381cb"
}

resource "keycloak_role" "video_manager_server_edit_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.video_manager_server_prod.id
  name = local.roles_common.edit_name
}

import {
  to = keycloak_role.video_manager_server_scan_role_prod
  id = "apps-prod/317935b1-6ed3-4917-8f89-a5f6158a50c9"
}

resource "keycloak_role" "video_manager_server_scan_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.video_manager_server_prod.id
  name = local.roles_common.scan_name
}

import {
  to = keycloak_role.video_manager_server_admin_role_prod
  id = "apps-prod/3dbd7e57-bd2e-458e-84b4-d368808e5820"
}

resource "keycloak_role" "video_manager_server_admin_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.video_manager_server_prod.id
  name = local.roles_common.admin_name
}