package io.craigmiller160.videomanagerserver.test_util

import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import us.craigmiller160.testcontainers.common.TestcontainersExtension

@ExtendWith(TestcontainersExtension::class, SpringExtension::class)
@SpringBootTest
annotation class VideoManagerIntegrationTest()
