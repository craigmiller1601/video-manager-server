package io.craigmiller160.videomanagerserver.test_util

import java.sql.Types
import org.hibernate.dialect.H2Dialect

class ExtendedH2Dialect : H2Dialect() {
  init {
    registerColumnType(Types.TIMESTAMP, "timestamp(9)")
  }
}
